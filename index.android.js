
 import React, { Component } from 'react';
 import {  AppRegistry } from 'react-native';
 import MyNavigator from './components/AppNavigator';
 import { Provider } from 'react-redux';
 import store from './store';
 import WelcomeScreen from './screens/WelcomeScreen';

 export default class Up2017 extends Component {
  render() {
    return (
      <Provider store={store} navigator={MyNavigator}>
        <MyNavigator />
      </Provider>
    );
  }
}
AppRegistry.registerComponent('Up2017', () => Up2017);
