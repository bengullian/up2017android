import React, { Component } from 'react';
import { ActivityIndicator, View, StyleSheet,
         Text, Dimensions, ScrollView, Image,
         StatusBar,
      } from 'react-native';
import CustomCard from './CustomCard';
import MessageBarAlert from 'react-native-message-bar/MessageBar';
import MessageBarManager from 'react-native-message-bar/MessageBarManager';
const {width, height}= Dimensions.get('window').width;

class ArtWorkSlides extends React.Component {
  constructor(props) {
    super(props);
  };

  componentDidMount() {
  MessageBarManager.registerMessageBar(this.refs.alert);
  MessageBarManager.showAlert({
  title: 'Swipe left for more Art Works',
  alertType: 'custom',
  stylesheetExtra : { backgroundColor : '#2e004d', strokeColor : 'gray' },
  duration: 3000,
});
}

componentWillUnmount() {
  MessageBarManager.unregisterMessageBar();
}

  renderSlides(dataAr) {
    return dataAr.map((item) => {
      var ev_Start = item.start_date.substr(0,4)+"-"+item.start_date.substr(4,2)+"-"+item.start_date.substr(6,2);
      var ev_End = item.end_date.substr(0,4)+"-"+item.end_date.substr(4,2)+"-"+item.end_date.substr(6,2);
      return(
        <ScrollView vertical key={item.id}>
           <CustomCard
              event={item}
              nav={this.props.nav}
              key={item.id}
              title={item.title}
              image={item.featured_image}
              artist={item.artist}
              short_intro={item.short_intro}
              desc_1={item.description_1}
              start_date={ev_Start}
              stop_date={ev_End}
           />
       </ScrollView>
      );
    });
  }

  render(){
    var dataAr = [];
    for(var i in this.props.data) {
      dataAr.push(this.props.data[i]);
    }
    if(! dataAr.length > 0){
      return(
      <View style={styles.container}>
          <Text style={{fontSize: 14}}>Loading, please wait</Text>
          <Text style={{fontSize: 14}}>An internet connection is needed to proceed</Text>
          <ActivityIndicator size="large" style={{top: 30}}/>
      </View>
    );
    }
    return(
      <View style={styles.container}>
        <ScrollView horizontal pagingEnabled >
          { this.renderSlides(dataAr) }
        </ScrollView>
        <MessageBarAlert ref="alert" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  wrapper: {
  },
  container: {
    flex: 1,
    width: width,
    height: height,
    backgroundColor: '#e6e6ff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ArtWorkSlides;
