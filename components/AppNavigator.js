import React from 'react';
import { Text, View, Image,  } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AboutScreen from '../screens/AboutScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import ArtWorksScreen from '../screens/ArtWorksScreen';
import EventsScreen from '../screens/EventsScreen';
import CalendarScreen from '../screens/CalendarScreen';
import MapScreen from '../screens/MapScreen';
import WaitingScreen from '../screens/WaitingScreen';
import ImageHeader from './ImageHeader';
import ArtWork from './ArtWork';

const getHeaderTopPadding = () => {
  return 10;
}

const ShowAbout = ({navigation}) => {
  return <AboutScreen navigation={navigation}/>;
}

ShowAbout.navigationOptions = ({navigation}) => {
  return {
    drawerLabel: 'Om UP!',
    drawerIcon: () => (
      <MaterialIcons
        name="info-outline"
        size={25}
        style={{ color: '#80b3ff' }}
      />
    ),
    header: () =>{
       return <ImageHeader title= {'Art Works'} nav={{navigation}} menu={true}  />
    },
  }
};

const getHeaderRight = (navigation)  => {
  return (
    <MaterialIcons
      name="menu"
      size={35}
      style={{ color: '#404c1a' }}
      onPress={ () => navigation.navigate('DrawerOpen') }
    />
  );
}

const ShowMap = ({navigation}) => {
  return <MapScreen navigation={navigation}/>;
}

ShowMap.navigationOptions = ({navigation}) => {
  return {
    drawerLabel: 'Map',
    drawerIcon: () => (
      <MaterialIcons
        name="place"
        size={24}
        style={{ color: '#008fb3' }}
      />
    ),
    headerRight: getHeaderRight(navigation),
    headerLeft: (
      <Image source={require('../assets/images/UP_LOGO_48.png')} style={styles.imageStyle}/>
    ),
    headerStyle: [styles.headerStyle,{backgroundColor: '#ecf6f9'}],
  }
};

const ShowArtWorks = ({navigation}) => {
  return <ArtWorksScreen navigation={navigation}/>;
}


ShowArtWorks.navigationOptions = ({navigation}) => {
  return {
    drawerLabel: 'Kunstværker',
    drawerIcon: ({ tintColor }) => (
      <MaterialIcons
        name="collections"
        size={24}
        style={{ color: '#996666' }}
      />
    ),
    headerRight: getHeaderRight(navigation),
    headerLeft: (
      <Image source={require('../assets/images/UP_LOGO_48.png')} style={styles.imageStyle}/>
    ),
    headerStyle: styles.headerStyle,
  }
}


const ShowCalendar = ({navigation}) => {
  return <CalendarScreen navigation={navigation}/>;
}

ShowCalendar.navigationOptions = ({navigation}) => {
  return {
    drawerLabel: 'Kalender',
    drawerIcon: ({ tintColor }) => (
      <MaterialIcons
        name="date-range"
        size={27}
        style={{ color: '#334d00' }}
      />
    ),
    headerRight: getHeaderRight(navigation),
    headerLeft: (
      <Image source={require('../assets/images/UP_LOGO_48.png')} style={styles.imageStyle}/>
    ),
    headerStyle: [styles.headerStyle, {backgroundColor: '#eef5f7'}],
  }
};


const ShowWelcomeScreen = ({navigation}) => {
  return <WelcomeScreen navigation={navigation} />;
}

ShowWelcomeScreen.navigationOptions = () => {
  return {
  headerLeft: (
    <Image source={require('../assets/images/UP_LOGO_48.png')} style={styles.imageStyle}/>
  ),
  headerStyle: [styles.headerStyle, {backgroundColor: '#eef5f7'}],
}
};

const _ArtWork = ({navigation}) => {
  return <ArtWork navigation={navigation}/>;
}

_ArtWork.navigationOptions = ({navigation}) => {
   return{
    title: navigation.state.params.title,
    headerStyle: styles.headerStyle,
  }
}

const ShowEvents = ({navigation}) => {
  return <EventsScreen navigation={navigation}/>;
}

ShowEvents.navigationOptions = ({navigation}) => {
  return {
    drawerLabel: 'Begivenheder',
    drawerIcon: () => (
      <MaterialIcons
        name="event-available"
        size={25}
        style={{ color: '#b37700' }}
      />
    ),
    header: () =>{
      return <ImageHeader title= {'Art Works'} nav={{navigation}} menu={true}  />
      },
  }
};

const ShowWaitingScreen = ({navigation}) => {
  return <WaitingScreen navigation={navigation} />;
}

const AppNavigator = StackNavigator({

  loading: {
    screen: ShowWaitingScreen,
  },
  work: {
    screen: _ArtWork,
  },
  welcome: {
    screen: ShowWelcomeScreen,
  },
  main: {
    screen: DrawerNavigator(
      {
        artworks: { screen: ShowArtWorks },
        calendar: { screen: ShowCalendar },
        map: { screen: ShowMap },
        about: { screen: ShowAbout },
        events: { screen: ShowEvents },
      },
      {
        drawerWidth: 230,
        style: {
          fontSize: 18,
        },
        contentOptions: {
          activeTintColor: '#e91e63',
        },
      }
    ),
  },

},
{
  lazy: false
}
);

const styles = {
  headerStyle: {
    paddingLeft: 5,
    paddingRight: 7,
    paddingTop: getHeaderTopPadding(),
    paddingBottom: 5,
    height: 50,
    backgroundColor: '#ededf8',

  },
  imageStyle: {
    width: 40,
    height: 40,
    resizeMode: 'center',
  }
};

export default AppNavigator;
