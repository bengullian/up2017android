import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity, StatusBar  } from 'react-native';
import EventForList from './EventForList';
import LinearGradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('window');
const TEXT_COLOR = '#656565';

class EventList extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    const event = this.props.event;
    const eventStart = event.start_date.substr(0,4)+"-"+event.start_date.substr(4,2)+"-"+event.start_date.substr(6,2);
    const eventEnd = event.end_date.substr(0,4)+"-"+event.end_date.substr(4,2)+"-"+event.end_date.substr(6,2);

    return (
      <LinearGradient
        colors={['#fdefe7', '#f9d0b9', '#fdefe7']}
        style={{padding: 2, borderWidth: 5, borderColor: '#fdefe7',  alignItems: 'center'}}
        start={{x: 0.1, y: 0.9}}
        end={{x: 0.9, y: 0.9}}
      >
      <TouchableOpacity
        style={{flex: 1, width: width*0.9,  paddingBottom: 15}}
        onPress={ () => { this.props.nav.navigate('work', {title: event.title, event:event}) }}
      >
        <View style={styles.eventItem}>
          <View style={styles.eventTextContainer}>
             <Text style={[styles.eventTextStyle, {fontSize: 14, fontFamily: 'avenir_next_medium'}]}>
               {eventStart}{' til '}{eventEnd}
             </Text>
            <Text style={[styles.eventTextStyle, {fontSize: 16, fontWeight: '400', fontFamily: 'avenir_next_medium'} ]}>{event.title.trim()}</Text>
            <Text style={[styles.eventTextStyle, {fontSize: 14, fontFamily: 'avenir_next_medium'}]}>{event.venue.trim()}</Text>
            <Text>
              { event.time_open === "" ? null : (
                <Text style={[styles.eventTextStyle, {fontSize: 14, fontFamily: 'avenir_next_medium'}]}>
                  {'kl. '}{event.time_open}</Text>)
              }
            </Text>
          </View>
            <Image
              source={{uri: event.featured_image}}
              style={{width: 100, height: 80, resizeMode: 'center', }}
             />
        </View>
      </TouchableOpacity>
      </LinearGradient>
    );
  };
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdefe7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  eventItem: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 5,
    paddingRight: 7,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    backgroundColor: 'transparent',
 },
 eventTextContainer: {
   flex: 1,
   alignItems: 'flex-start',
   justifyContent: 'flex-start',
   backgroundColor: 'transparent',
 },
 eventTextStyle: {
   paddingBottom: 5,
   color: TEXT_COLOR,
   fontSize: 12,
 },
});
export default EventList;
