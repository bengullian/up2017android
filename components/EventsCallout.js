import React, { Component } from 'react';
import { View, Text, } from 'react-native';

class EventsCallout extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var target = this.props.event;
    var ev_Start = target.start_date.substr(0,4)+"-"+target.start_date.substr(4,2)+"-"+target.start_date.substr(6,2);
    var ev_End = target.end_date.substr(0,4)+"-"+target.end_date.substr(4,2)+"-"+target.end_date.substr(6,2);

   return (
     <View>
       <Text>{this.props.event.title}</Text>
     </View>
   );
};
}
export default EventsCallout;
