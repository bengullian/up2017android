import React, { Component } from 'react';
import { Dimensions, } from 'react-native';
import XDate from 'xdate';
import { Calendar } from 'react-native-calendars';

const SCREEN_WIDTH = Dimensions.get('window').width;

class MyCalendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        current: {},
        selectedDate: [],
        marked: {},
        dateToHighlight: {},
        lastSelected: [],
        };
    }

    componentWillMount() {
        var startDate = new XDate(this.props.start);
        var stopDate = new XDate(this.props.end);
        var daysDiff = startDate.diffDays(stopDate);
        for(var i = 1; i<=daysDiff+1; i++) {
            var dateISOString = startDate.clone().addDays(i).toISOString();
            var dateOnly = dateISOString.slice(0, dateISOString.indexOf('T'));
            this.state.marked[dateOnly] = {selected: false, marked: true};
        }
        this.state.dateToHighlight = this.state.marked;
    }

    componentWillReceiveProps(NextProps) {
        if(NextProps.events) {
            this.setState({ events: NextProps.events });
        }
    }

    onDateSelect = (date) =>{      
        var year = date.year;
        var month = parseInt(date.month)<10 ? "0"+date.month : date.month;
        var day = parseInt(date.day)<10 ? "0"+date.day : date.day
        var selected = year +'-'+ month +'-'+ day;

        //to get the selected day from this component in the parent component
        this.props.getSelectedDay(selected);

        var tmp = {};
        tmp[selected] = {'selected': true};

        if(this.state.marked[selected]) {
            this.state.marked[selected].selected = true;
        }
        if(this.state.lastSelected.length > 0 ) {
            if(this.state.marked[this.state.lastSelected[0]]) {
                if(this.state.lastSelected[0] == selected) {
                    this.state.marked[this.state.lastSelected[0]].selected = true
                }else {
                    this.state.marked[this.state.lastSelected[0]].selected = false;
                }
            }
        }
        this.state.lastSelected[0] = selected;
        var newTmp = {};
        Object.assign(newTmp, tmp, this.state.marked);
        this.setState({selectedDate: selected, dateToHighlight: newTmp, current: date});
    }


    render() {
        return (
                <Calendar
                style={{
                borderBottomWidth: 1,
                borderColor: '#c1d7d7',
                width: SCREEN_WIDTH,

                }}
                current={this.state.current}
                theme={{
                calendarBackground: '#ffffff',
                textSectionTitleColor: '#b6c1cd',
                selectedDayBackgroundColor: '#00adf5',
                selectedDayTextColor: '#ffffff',
                todayTextColor: '#00adf5',
                dayTextColor: '#2d4150',
                textDisabledColor: '#d9e1e8',
                dotColor: '#00adf5',
                selectedDotColor: '#ffffff',
                arrowColor: 'orange',
                monthTextColor: '#00b300'
                }}
                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                firstDay={0}
                hideExtraDays={false}
                // Handler which gets executed on day press. Default = undefined
                onDayPress={(date) => this.onDateSelect(date)} // Callback after date selectionPropTypes.func,
                monthFormat={'MMMM yyyy'}
                markedDates={this.state.dateToHighlight}
                />
                );
    }
}
export default MyCalendar;
