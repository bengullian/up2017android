import React,{Component} from 'react';
import {View, Image, StyleSheet, Dimensions } from 'react-native';
import  MaterialIcons  from 'react-native-vector-icons/MaterialIcons';

const SCREEN_WIDTH = Dimensions.get('window').width;

class ImageHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  menuImage = (nav) => {
     return (
             <MaterialIcons
             name="menu"
             size={35}
             style={{ color: '#404c1a', backgroundColor: 'transparent' }}
             onPress={ () => nav.navigation.navigate('DrawerOpen') }
             />
      );
  }

  render() {
    return (
    <View style={{width: SCREEN_WIDTH , height: 50,}}>
      <Image source={require('../assets/images/top_menu_background_sky.png')} style={styles.backgroundImage}>
         <View style={styles.headerStyle}>
         <Image source={require('../assets/images/UP_LOGO_48.png')} style={{width:40, height:40, resizeMode: 'center'}}/>
          { this.menuImage(this.props.nav)}
         </View>
      </Image>
    </View>
    );
  };
}

const styles = StyleSheet.create({
    backgroundImage: {
     flex: 1,
     width: undefined,
     height: undefined,
     resizeMode: 'cover',
    },
    headerStyle: {
        flex: 1,
        paddingLeft: 7,
        paddingRight: 7,
        paddingTop: 5,
        flexDirection: 'row',
         alignItems: 'center',
        justifyContent: 'space-between',
    }
});

export default ImageHeader;
