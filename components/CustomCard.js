import React, { Component } from 'react';
import {
  Dimensions, View, TouchableOpacity,
  ScrollView, StyleSheet, Text, Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ArtWork from './ArtWork'

const {width, height} = Dimensions.get('window');
const TEXT_COLOR = '#656565';

class CustomCard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <View style={styles.container}>
        <View style={styles.titleBar}>
          <Text style={[styles.headerStyle, {fontFamily: 'Avenir Next Medium'}, {fontWeight: '600'}]}>{this.props.title.trim()}</Text>
          <Text style={[styles.textGroupStyle, {fontFamily: 'Avenir Next Regular' }]} numberOfLines={2}>{this.props.short_intro.trim()}</Text>
          <Text style={[styles.textGroupStyle, {fontFamily: 'Avenir Next Regular' }]}>{'By '}{this.props.artist.trim()}</Text>
          <Text style={[styles.textGroupStyle, {fontFamily: 'Avenir Next Regular' }]}>{this.props.start_date.trim()}{' til '}{this.props.stop_date.trim()}</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image source={{uri: this.props.image}} style={styles.imageStyle} />
          <Text numberOfLines={6} style={[styles.textStyle, {fontFamily: 'Avenir Next Regular' }]}>{this.props.desc_1}</Text>
          <Text numberOfLines={6} style={[styles.textStyle, {fontFamily: 'Avenir Next Regular' }]}>{this.props.desc_2}</Text>
          <LinearGradient
            colors={['#e6eaff', '#ac98b3', '#e6e6ff']}
            style={{padding: 10, alignItems: 'center', borderRadius: 5}}
          >
             <TouchableOpacity onPress={() => { this.props.nav.navigate('work', {title: this.props.title, event: this.props.event }) } }>
               <Text style={{backgroundColor: 'transparent', fontSize: 15, color: 'white', fontFamily: 'Avenir Next Medium' }}>
                Læs mere
               </Text>
             </TouchableOpacity>
          </LinearGradient>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    backgroundColor: '#e6e6ff',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  titleBar: {
    paddingTop: 10,
    width: width * 0.9,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  imageContainer: {
    flex: 1,
    backgroundColor: '#e6e6ff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingBottom: 50,
  },
  imageStyle: {
    width: width * 0.9,
    height: 200,
    resizeMode: 'stretch',
  },
  textStyle: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
    width: width*0.9,
    color: TEXT_COLOR,
  },
  headerStyle : {
    paddingTop: 5,
    color: TEXT_COLOR,
    fontSize: 18,
  },
  textGroupStyle : {
    paddingTop: 5,
    color: TEXT_COLOR,
    fontSize: 14,
  },
});
export default CustomCard;
