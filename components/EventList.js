import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet,  FlatList  } from 'react-native';
import EventForList from './EventForList';

const {width, height}= Dimensions.get('window');
const TEXT_COLOR = '#656565';

class EventList extends React.Component {
  constructor(props){
    super(props);
  }

  renderEvent = ({item}) => {
      return(
        <EventForList key={item.id} event={item} nav={this.props.nav} />
      );
   }

  render() {
    var dataAr = [];
    for(var i in this.props.data) {
      dataAr.push(this.props.data[i]);
    }
      return (
        <View style={styles.listView}>
         <FlatList
           data={dataAr}
           renderItem={this.renderEvent}
           keyExtractor={(item)=> item.id}
           vertical
         />
        </View>
      );
  };
}
const styles = StyleSheet.create({
  listView: {
    flex: 1,
    width,
    height,
    alignItems: 'center',
    backgroundColor: '#fdefe7',
  },
});
export default EventList;
