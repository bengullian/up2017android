import React, { Component } from 'react';
import { StyleSheet, View, Image, } from 'react-native';

class BackgroundImage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Image
        source={require('../assets/images/welcome_screen_sky.png')}
        style={styles.backgroundImage}
      >
      {this.props.children}
      </Image>
    ) ;
  };
}

const styles = StyleSheet.create({
    backgroundImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    },
});

export default BackgroundImage;
