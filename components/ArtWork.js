import React, { Component } from 'react';
import { Dimensions, View, ScrollView, StyleSheet, StatusBar, Text, Image, } from 'react-native';

const {width, height} = Dimensions.get('window');
const TEXT_COLOR = '#656565';

class ArtWork extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
     var target = this.props.navigation.state.params.event;
     var ev_Start = target.start_date.substr(0,4)+"-"+target.start_date.substr(4,2)+"-"+target.start_date.substr(6,2);
     var ev_End = target.end_date.substr(0,4)+"-"+target.end_date.substr(4,2)+"-"+target.end_date.substr(6,2);
    return(
      <ScrollView style={{backgroundColor: '#e6e6ff'}}>
        <View style={styles.container}>
          <View style={styles.titleBar}>
            <Text style={[styles.textGroupStyle, {fontFamily: 'Avenir Next Medium'}]}>{target.short_intro.trim()}</Text>
            <Text style={[styles.textGroupStyle, {fontFamily: 'Avenir Next Regular'}]}>{'By '}{target.artist}</Text>
            <Text style={[styles.textGroupStyle,{fontFamily: 'Avenir Next Regular'}]}>{target.venue.trim()}</Text>
          </View>
          <View style={styles.imageContainer}>
            <Image source={{uri: target.featured_image}} style={styles.imageStyle} />
            <Text  style={[styles.textStyle, {fontFamily: 'Avenir Next Regular'}] }>{target.description_1}</Text>
            <Text  style={[styles.textStyle, {fontFamily: 'Avenir Next Regular'}] }>{target.description_2}</Text>
            <Text  style={[styles.textStyle, {fontFamily: 'Avenir Next Regular'}] }>{target.description_3}</Text>
            <Text  style={[styles.textStyle, {fontFamily: 'Avenir Next Regular'}] }>{target.other_relevant_information}</Text>
          </View>
        </View>
      </ScrollView>
   );
};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6e6ff',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  titleBar: {
    top: 10,
    width: width * 0.8,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#e6e6ff',
  },
  imageContainer: {
    backgroundColor: '#e6e6ff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 20,
  },
  imageStyle: {
    width: width * 0.8,
    height: 120,
    resizeMode: 'stretch',
    backgroundColor: '#e6e6ff',
  },
  textStyle: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
    width: width * 0.8,
    color: TEXT_COLOR,
    backgroundColor: '#e6e6ff',
  },
  textGroupStyle : {
    marginBottom: 10,
    alignItems: 'center',
    width: width * 0.8,
    color: TEXT_COLOR,
    backgroundColor: '#e6e6ff',
  },
});
export default ArtWork;
