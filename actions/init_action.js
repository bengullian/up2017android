import axios  from 'axios';
import {
  RECEIVED_APP_DATA,
  APP_DATA_REQUEST_FAILED
} from './types';


const API_ROUTE = "http://up2017.dk/?rest_route=/up2017/events";

export const init_action = () => async (dispatch) => {
    axios.get(API_ROUTE)
       .then((response) => {
         console.log("data request ok")
         return dispatch({ type: RECEIVED_APP_DATA, payload: response.data });
       })
       .catch((error) => {
         console.log("An error occured", error);
         return dispatch({ type: APP_DATA_REQUEST_FAILED, isConnected: false });
       })
};
