
import {
  CONNECTION_STATUS,
} from './types';

export const conn_action = ({ status }) => async (dispatch) => {
  return dispatch({ type: CONNECTION_STATUS, isConnected: status });
};
