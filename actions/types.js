export const RECEIVED_APP_DATA = 'received_app_data';
export const APP_DATA_REQUEST_FAILED = 'app_data_request_failed';
export const GET_APP_DATA = 'get_app_data_from_store';
export const CONNECTION_STATUS = 'app_connection_status';
